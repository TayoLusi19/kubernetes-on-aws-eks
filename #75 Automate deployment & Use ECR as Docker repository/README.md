Automating deployment to your EKS cluster through Jenkins and switching your Docker repository to Amazon Elastic Container Registry (ECR) involves several steps but significantly streamlines your CI/CD pipeline. Here's how to achieve this, broken down into manageable tasks:

## Automate Deployment with Jenkins
1. Create an ECR Registry for Your Java App Image:
- Go to the Amazon ECR service in AWS Management Console.
- Create a new repository named java-app or similar.

2. Configure Docker Registry Secret for ECR in Kubernetes:
First, retrieve your ECR login password using AWS CLI on your local machine or wherever you have AWS CLI access:

```bash
aws ecr get-login-password --region {ecr-region}
```

Then, create a Kubernetes secret in the my-app namespace to allow your Kubernetes cluster to pull images from ECR:

```bash
kubectl create secret docker-registry my-ecr-registry-key \
--docker-server={your-aws-id}.dkr.ecr.{your-ecr-region}.amazonaws.com \
--docker-username=AWS \
--docker-password={ecr-login-password} \
-n my-app
```

3. Configure Jenkins Environment:
SSH into your server where Jenkins is running:

```bash
ssh -i {private-key-path} {user}@{public-ip}
```

Enter the Jenkins container:

```bash
sudo docker exec -it {jenkins-container-id} bash
```

## 4. Install AWS CLI and kubectl in Jenkins Container:

Follow the provided links to install the AWS CLI and kubectl inside your Jenkins container. These tools are essential for interacting with AWS services and your Kubernetes cluster.

## 5. Install envsubst:

```bash
apt-get update
apt-get install -y gettext-base
```

## 6. Configure AWS Credentials in Jenkins:

Create "secret-text" credentials in Jenkins for AWS access:
- jenkins_aws_access_key_id: For AWS_ACCESS_KEY_ID.
- jenkins_aws_secret_access_key: For AWS_SECRET_ACCESS_KEY.

## 7. Configure Database Secrets in Jenkins:

Create "secret-text" credentials in Jenkins for your database configuration used in db-secret.yaml.

8. Set Environment Variables in Jenkins:

Configure the ECR_REPO_URL and CLUSTER_REGION environment variables in Jenkins to match your AWS setup.

9. Update Jenkins Pipeline:

Modify your Jenkinsfile to include stages for building your Docker image, pushing it to ECR, and deploying your application to EKS. Ensure the deploy stage updates Kubernetes deployment manifests with the new image from ECR and applies them to your cluster.

## Example Jenkins Pipeline Modifications:

```bash
pipeline {
    // Define other stages
    stage('Build and Push to ECR') {
        steps {
            script {
                // Login to ECR, build image, tag and push to ECR
            }
        }
    }
    stage('Deploy to EKS') {
        steps {
            script {
                // Update Kubernetes manifests and apply
            }
        }
    }
}
```

Note: For the deployment stage, consider using a script that updates the image in your Kubernetes deployment manifest with the latest image pushed to ECR. The envsubst command can be useful for substituting environment variable values into your manifest files before applying them with kubectl.

By following these steps, you automate the deployment process of your Java application on EKS, leveraging Jenkins for CI/CD and AWS ECR for container image storage, thus achieving a more efficient and reliable deployment workflow.

# Contributions
Contributions to this script are welcome. Please ensure to follow good coding practices and provide a detailed description of your changes or improvements.

# License
[LICENSE.md](https://gitlab.com/TayoLusi19/kubernetes-on-aws-eks/-/blob/main/LICENSE.md?ref_type=heads)

# Author
Adetayo Michael Ibijemilusi











