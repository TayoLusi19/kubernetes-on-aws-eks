Deploying your Java application on AWS Fargate using Amazon EKS allows you to run containers without managing servers or clusters. By using the my-app namespace, which is associated with your Fargate profile, you ensure that your application utilizes the serverless computing infrastructure provided by Fargate. Here's how to deploy your Java application with all necessary configurations and secrets, ensuring it runs with 3 replicas for high availability.

## Step 1: Create the Application Namespace
First, create the my-app namespace to organize your Java application resources. This namespace should be associated with the Fargate profile you've previously created.

```bash
kubectl create namespace my-app
```

## Step 2: Create Docker Registry Secret
Before deploying your application, you'll need to create a Docker registry secret to allow Kubernetes to pull your private image from Docker Hub. Replace the placeholders with your actual Docker credentials.

```bash
DOCKER_REGISTRY_SERVER=docker.io
DOCKER_USER=<your_dockerID>
DOCKER_EMAIL=<your_dockerhub_email>
DOCKER_PASSWORD=<your_dockerhub_password>

kubectl create secret docker-registry my-registry-key \
--docker-server=$DOCKER_REGISTRY_SERVER \
--docker-username=$DOCKER_USER \
--docker-password=$DOCKER_PASSWORD \
--docker-email=$DOCKER_EMAIL \
-n my-app
```

## Step 3: Deploy Configuration and Secrets
With the Docker registry secret in place, next, apply your application's configuration and secrets within the my-app namespace. This ensures that your application's environment variables and sensitive information are correctly set up in Fargate.

From your k8s-deployment folder:

```bash
kubectl apply -f db-secret.yaml -n my-app
kubectl apply -f db-config.yaml -n my-app
```

## Step 4: Deploy Your Java Application
Finally, deploy your Java application. The java-app.yaml should define your Deployment and Service resources, and possibly an Ingress resource if you're exposing your application externally. Make sure this YAML file is configured to pull your application's Docker image, set the desired number of replicas to 3, and uses the my-app namespace.

```bash
kubectl apply -f java-app.yaml -n my-app
```

## Verification
After applying the manifests, you can verify the deployment, pods, and services are correctly created in the my-app namespace:

```bash
kubectl get all -n my-app
```

This command will list all resources within the my-app namespace, showing your application's deployment status, the pods, and any services or ingresses defined in your java-app.yaml.

## Accessing Your Application
Directly via Service: If your service is of type LoadBalancer (and you're on a supported platform like AWS), you'll get an external IP to access your application.

Via Ingress: If you've configured an Ingress, ensure your DNS is set up to point to the Ingress controller's IP, and access your application using the domain specified in the Ingress configuration.

Deploying on Fargate provides a scalable and serverless backend for your Java application, simplifying operations and allowing you to focus on development rather than infrastructure management.

# Contributions
Contributions to this script are welcome. Please ensure to follow good coding practices and provide a detailed description of your changes or improvements.

# License
[LICENSE.md](https://gitlab.com/TayoLusi19/kubernetes-on-aws-eks/-/blob/main/LICENSE.md?ref_type=heads)

# Author
Adetayo Michael Ibijemilusi
