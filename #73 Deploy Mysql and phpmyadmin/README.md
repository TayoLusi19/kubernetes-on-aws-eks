Deploying MySQL and phpMyAdmin on EC2 nodes within your Amazon EKS cluster involves a few steps, including setting up Helm for MySQL and applying Kubernetes manifests for both MySQL and phpMyAdmin. Here's a detailed guide based on the instructions provided:

## Step 1: Clone the Repository and Set Up
First, clone the provided Git repository and switch to the appropriate branch and directory that contains the Kubernetes deployment manifests:

```bash
git clone https://gitlab.com/twn-devops-bootcamp/latest/11-eks/eks-exercises.git
cd eks-exercises
git checkout feature/solutions
cd k8s-deployment
```

## Step 2: Install MySQL Using Helm
You'll use the Bitnami MySQL Helm chart for a straightforward MySQL deployment. Before proceeding, ensure you have Helm installed and configured to work with your EKS cluster.

Add Bitnami Helm Repository:

```bash
helm repo add bitnami https://charts.bitnami.com/bitnami
helm repo update
```

Deploy MySQL:
Deploy the MySQL chart with a custom values file tailored for EKS (mysql-chart-values-eks.yaml). This file should configure MySQL to suit your requirements, including storage options, replica count, and any other necessary parameters.

```bash
helm install my-release bitnami/mysql -f mysql-chart-values-eks.yaml
```

## Step 3: Deploy phpMyAdmin
After installing MySQL, you'll deploy phpMyAdmin, which requires a ConfigMap and a Secret for its configuration, and access to the MySQL database.

Deploy ConfigMap and Secret:
These resources contain the necessary configuration and credentials for phpMyAdmin to connect to your MySQL instance.

```bash
kubectl apply -f db-config.yaml
kubectl apply -f db-secret.yaml
```

Deploy phpMyAdmin:
Apply the phpMyAdmin Kubernetes manifest to create the necessary Deployment and Service.

```bash
kubectl apply -f phpmyadmin.yaml
```

## Step 4: Access phpMyAdmin
To access phpMyAdmin securely without exposing it to the internet, use port forwarding:

```bash
kubectl port-forward svc/phpmyadmin-service 8081:8081
```

This command forwards port 8081 on your local machine to the phpMyAdmin service in your cluster, allowing you to access phpMyAdmin through localhost:8081.

## Step 5: Log In to phpMyAdmin
After accessing phpMyAdmin in your browser, log in using the MySQL credentials you have defined:

Username: my-user / Password: my-pass
Or, for MySQL root access: Username: root / Password: secret-root-pass
Ensure these credentials match what you've set in your db-secret.yaml or the MySQL Helm chart values.

## Conclusion
By following these steps, you've successfully deployed a MySQL database and phpMyAdmin within your Amazon EKS cluster on EC2 nodes. This setup provides you with a robust database service and a convenient web interface for database management, all running securely within your Kubernetes environment.

# Contributions
Contributions to this script are welcome. Please ensure to follow good coding practices and provide a detailed description of your changes or improvements.

# License
[LICENSE.md](https://gitlab.com/TayoLusi19/kubernetes-on-aws-eks/-/blob/main/LICENSE.md?ref_type=heads)

# Author
Adetayo Michael Ibijemilusi
