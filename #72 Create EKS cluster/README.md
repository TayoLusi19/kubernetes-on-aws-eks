Creating an Amazon EKS (Elastic Kubernetes Service) cluster using eksctl is a straightforward process that abstracts much of the complexity involved in setting up an EKS cluster. Here's a step-by-step guide to get your EKS cluster up and running with EC2 instances for worker nodes and a Fargate profile for serverless computing capabilities. This setup allows you to take advantage of both the managed node groups for workloads requiring specific EC2 configurations and the serverless Fargate profiles for simpler, on-demand workload management.

## Step 1: Install eksctl
Before you begin, ensure that eksctl is installed on your local machine. Follow the installation guide provided in the [AWS documentation](https://docs.aws.amazon.com/eks/latest/userguide/setting-up.html) for detailed instructions.

## Step 2: Create Your EKS Cluster
Create the Cluster with EC2 Instances:
Use eksctl to create a new cluster named "my-cluster" with 3 EC2 instances as worker nodes. The --kubeconfig flag specifies a file to store the cluster access configuration.

```bash
eksctl create cluster --name=my-cluster --nodes=3 --kubeconfig=./kubeconfig.my-cluster.yaml
```

This command initiates the creation of an EKS cluster managed by AWS. It might take several minutes to complete.

Create a Fargate Profile:
After the cluster is created, add a Fargate profile to allow Kubernetes pods to run on AWS Fargate. This profile is associated with the "my-app" namespace, meaning any pods deployed in "my-app" will use Fargate.

```bash
eksctl create fargateprofile \
    --cluster my-cluster \
    --name my-fargate-profile \
    --namespace my-app
```

Note: If the "my-app" namespace doesn't exist yet, it will be created when you deploy applications to it, and the Fargate profile will automatically apply.

## Step 3: Configure kubectl to Use Your EKS Cluster
Set KUBECONFIG Environment Variable:
Point kubectl to your newly created cluster by setting the KUBECONFIG environment variable to the path of your kubeconfig file. Replace {absolute-path} with the actual path to kubeconfig.my-cluster.yaml.

```bash
export KUBECONFIG={absolute-path}/kubeconfig.my-cluster.yaml
```

Validate Cluster Accessibility:
Confirm that kubectl can access your EKS cluster and see the EC2 worker nodes:

```bash
kubectl get nodes
```

To check the status of your Fargate profile, use:

```bash
eksctl get fargateprofile --cluster my-cluster
```

## Next Steps
With your EKS cluster now set up with both EC2 and Fargate compute options, you're ready to deploy your applications. For workloads that need to run on EC2 instances, you can deploy them as usual, and they'll be scheduled on the EC2 worker nodes. For workloads that you wish to run on Fargate, ensure they're deployed in the "my-app" namespace to leverage the serverless computing environment.

This setup provides a flexible and scalable foundation for running a wide variety of workloads on EKS, balancing the control and customization of EC2 with the simplicity and scalability of Fargate.

# Contributions
Contributions to this script are welcome. Please ensure to follow good coding practices and provide a detailed description of your changes or improvements.

# License
[LICENSE.md](https://gitlab.com/TayoLusi19/kubernetes-on-aws-eks/-/blob/main/LICENSE.md?ref_type=heads)

# Author
Adetayo Michael Ibijemilusi
