
Configuring autoscaling for your Amazon EKS cluster can help optimize costs by adjusting the number of nodes in your cluster based on demand. EKS supports autoscaling through the Cluster Autoscaler, which automatically adjusts the size of your cluster when nodes are underutilized or when pods fail to launch due to insufficient resources.

## Step 1: Install the Cluster Autoscaler
To configure autoscaling, you'll first need to deploy the Cluster Autoscaler in your cluster. The Cluster Autoscaler's version must match the version of your Kubernetes cluster. Check the Cluster Autoscaler documentation for compatibility and detailed instructions.

1. Obtain the Cluster Autoscaler YAML file for your Kubernetes version from the official GitHub repository.

2. Edit the deployment YAML to include your cluster's name and the AWS Region it's deployed in. You'll need to modify the following command-line arguments in the Cluster Autoscaler deployment manifest:

```bash
        - command:
          - ./cluster-autoscaler
          - --v=4
          - --stderrthreshold=info
          - --cloud-provider=aws
          - --skip-nodes-with-local-storage=false
          - --expander=least-waste
          - --nodes={MIN_NODES}:{MAX_NODES}:{INSTANCE_GROUP_NAME}
          - --balance-similar-node-groups
          - --skip-nodes-with-system-pods=false
```

Replace {MIN_NODES}, {MAX_NODES}, and {INSTANCE_GROUP_NAME} with 1, 3, and the name(s) of your node group(s) or Auto Scaling group(s), respectively. For multiple node groups, repeat the --nodes parameter as needed.

3. Apply the modified Cluster Autoscaler YAML file to your cluster:

```bash
kubectl apply -f cluster-autoscaler-autodiscover.yaml
```

## Step 2: IAM Role and Policy for Cluster Autoscaler
The Cluster Autoscaler requires specific IAM permissions to modify the Auto Scaling groups. Ensure the IAM role associated with your EKS worker nodes has the necessary policies attached.

1. Attach the AmazonEKSClusterAutoscalerPolicy managed policy to the IAM role used by your worker nodes. This policy grants the required permissions for the Cluster Autoscaler to function.

2. If the managed policy doesn't cover all your needs, you may need to create a custom policy with additional permissions and attach it to the role.

## Step 3: Verify Autoscaling Configuration
After deploying the Cluster Autoscaler and configuring IAM roles and policies, monitor your cluster to ensure it scales as expected:

```bash
kubectl get pods -n kube-system | grep autoscaler
```

Watch the Cluster Autoscaler logs for actions taken:

```bash
kubectl -n kube-system logs -f deployment.apps/cluster-autoscaler
```

## Best Practices and Considerations
- Overprovisioning: Consider overprovisioning your cluster to ensure that pods can be scheduled immediately without waiting for new nodes to be provisioned. The Cluster Autoscaler can then scale down excess nodes.
- Pod Disruption Budgets: Use Pod Disruption Budgets (PDBs) to ensure that critical applications maintain high availability during scaling operations.
- Testing: Test the autoscaling configuration under different loads to ensure it behaves as expected.
By following these steps, you can configure autoscaling for your EKS cluster, helping to optimize infrastructure costs while ensuring that your applications remain available and performant.

# Contributions
Contributions to this script are welcome. Please ensure to follow good coding practices and provide a detailed description of your changes or improvements.

# License
[LICENSE.md](https://gitlab.com/TayoLusi19/kubernetes-on-aws-eks/-/blob/main/LICENSE.md?ref_type=heads)

# Author
Adetayo Michael Ibijemilusi




